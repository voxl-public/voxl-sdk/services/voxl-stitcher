/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef CONFIG_FILE_H
#define CONFIG_FILE_H

#include <modal_json.h>
#include <c_library_v2/common/mavlink.h>

#define CONF_FILE "/etc/modalai/voxl-stitcher.conf"

#define CONFIG_FILE_HEADER "\
/**\n\
 * This file contains configuration that's specific to voxl-stitcher.\n\
 *\n\
 * width              - input image width\n\
 * height             - input image height\n\
 * input_pipe         - which camera to use.\n\
 * extrinsics_file    - location of cal files.\n\
 * intrinsics_file    - location of cal files.\n\
 * rc_channel         - channel of rc switch for overlay swapping\n\
 */\n"

static int width;
static int height;
static char input_pipe[MODAL_PIPE_MAX_DIR_LEN];
static char intrinsics_file[MODAL_PIPE_MAX_PATH_LEN];
static char extrinsics_file[MODAL_PIPE_MAX_PATH_LEN];
static int rc_channel;

static uint16_t (*rc_parser)(const mavlink_message_t* msg);

static void config_file_print(void) {
    printf("=================================================================\n");
    printf("width:                        %d\n", width);
    printf("height:                       %d\n", height);
    printf("input_pipe:                   %s\n", input_pipe);
    printf("intrinsics_file:              %s\n", intrinsics_file);
    printf("extrinsics_file:              %s\n", extrinsics_file);
    printf("rc_channel:                   %d\n", rc_channel);
    printf("=================================================================\n");
    return;
}

static int config_file_read(void)
{
	int ret = json_make_empty_file_with_header_if_missing(CONF_FILE, CONFIG_FILE_HEADER);
	if(ret < 0) return -1;
	else if(ret>0) fprintf(stderr, "Creating new config file: %s\n", CONF_FILE);

	cJSON* parent = json_read_file(CONF_FILE);
	if(parent==NULL) return -1;

	json_fetch_int_with_default(parent, "width", &width, 1280);
	json_fetch_int_with_default(parent, "height", &height, 800);

	json_fetch_string_with_default(	parent, "input_pipe",			input_pipe,				MODAL_PIPE_MAX_DIR_LEN, "stereo_front");
	json_fetch_string_with_default(	parent, "intrinsics_file",		intrinsics_file,		MODAL_PIPE_MAX_DIR_LEN, "/data/modalai/opencv_stereo_front_intrinsics.yml");
	json_fetch_string_with_default(	parent, "extrinsics_file",		extrinsics_file,		MODAL_PIPE_MAX_DIR_LEN, "/data/modalai/opencv_stereo_front_extrinsics.yml");

	json_fetch_int_with_default(parent, "rc_channel", &rc_channel, 6);

	switch (rc_channel)
	{
	case 1:
		rc_parser = &(mavlink_msg_rc_channels_get_chan1_raw);
		break;
	case 2:
		rc_parser = &(mavlink_msg_rc_channels_get_chan2_raw);
		break;
	case 3:
		rc_parser = &(mavlink_msg_rc_channels_get_chan3_raw);
		break;
	case 4:
		rc_parser = &(mavlink_msg_rc_channels_get_chan4_raw);
		break;
	case 5:
		rc_parser = &(mavlink_msg_rc_channels_get_chan5_raw);
		break;
	case 6:
		rc_parser = &(mavlink_msg_rc_channels_get_chan6_raw);
		break;
	case 7:
		rc_parser = &(mavlink_msg_rc_channels_get_chan7_raw);
		break;
	case 8:
		rc_parser = &(mavlink_msg_rc_channels_get_chan8_raw);
		break;
	case 9:
		rc_parser = &(mavlink_msg_rc_channels_get_chan9_raw);
		break;
	case 10:
		rc_parser = &(mavlink_msg_rc_channels_get_chan10_raw);
		break;
	case 11:
		rc_parser = &(mavlink_msg_rc_channels_get_chan11_raw);
		break;
	case 12:
		rc_parser = &(mavlink_msg_rc_channels_get_chan12_raw);
		break;
	case 13:
		rc_parser = &(mavlink_msg_rc_channels_get_chan13_raw);
		break;
	case 14:
		rc_parser = &(mavlink_msg_rc_channels_get_chan14_raw);
		break;
	case 15:
		rc_parser = &(mavlink_msg_rc_channels_get_chan15_raw);
		break;
	case 16:
		rc_parser = &(mavlink_msg_rc_channels_get_chan16_raw);
		break;
	case 17:
		rc_parser = &(mavlink_msg_rc_channels_get_chan17_raw);
		break;
	case 18:
		rc_parser = &(mavlink_msg_rc_channels_get_chan18_raw);
		break;
	default:
		rc_parser = nullptr;
		fprintf(stderr, "\nERROR: RECEIVED INVALID RC CHANNEL!\n\n");
		break;
	}

	if(json_get_parse_error_flag()){
		fprintf(stderr, "failed to parse config file %s\n", CONF_FILE);
		cJSON_Delete(parent);
		return -1;
	}

	// write modified data to disk if neccessary
	if(json_get_modified_flag()){
		json_write_to_file_with_header(CONF_FILE, parent, CONFIG_FILE_HEADER);
	}
	cJSON_Delete(parent);

	return 0;
}
#endif  // end CONFIG_FILE_H
