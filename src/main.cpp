/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <time.h>
#include <unistd.h>
#include <iostream>
#include <getopt.h>

#include <modal_pipe.h>
#include <modalcv.h>
#include <rc_math.h>

#include <opencv2/imgproc.hpp>
#include <opencv2/imgproc/types_c.h>
#include <opencv2/calib3d/calib3d.hpp>

#include "config_file.h"
#include "ai_detection.h"


#define PROCESS_NAME "voxl-stitcher"
#define IMAGE_CH 0
#define FLIR_PIPE    "flir"
#define DET_PIPE     "tflite_data"

#define FLIR_WIDTH  160
#define FLIR_HEIGHT 120

#define MAVLINK_IO_PATH (MODAL_PIPE_DEFAULT_BASE_DIR "vvpx4_mavlink_io/")

static bool en_flir_overlay = true;
static bool en_tflite_overlay = true;
static bool en_debug = false;

static mcv_lens_params_t lens1;
static mcv_lens_params_t lens2;

// different overlay modes we can output
// STITCHED: full stitched images with flir overlay
// NO_FLIR: bottom image only, no flir
// FLIR: bottom image only, flir overlay
enum OverlayMode { STITCHED, NO_FLIR, FLIR };
static OverlayMode curr_mode = STITCHED;

#define CHANNEL_STITCHED 1102
#define CHANNEL_NO_FLIR  1501
#define CHANNEL_FLIR     1898


cv::Mat M1, M2, M0, D1, D2, Mzoom, M1_warp, M2_warp;
static mcv_warp_handle warp1, warp2, warp3;

pthread_mutex_t flir_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t tflite_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t overlay_mode_mutex = PTHREAD_MUTEX_INITIALIZER;


static int last_frame_id_processed = -1;
std::vector<cv::Rect> tflite_detections;

cv::Mat flir_mat = cv::Mat(FLIR_HEIGHT*4, FLIR_WIDTH*4, CV_8UC3, cv::Scalar(255, 0, 0));


void _print_usage() {
    printf("\nCommand line arguments are as follows:\n\n");
    printf("-d, --debug            : Enable debug msgs\n");
    printf("-h, --help             : Print this help message\n");
}


static bool _parse_opts(int argc, char* argv[]) {
    static struct option long_options[] =
        {
            {"help",  no_argument, 0, 'h'},
            {"debug", no_argument, 0, 'd'},
            {0, 0, 0, 0}};

    while (1) {
        int option_index = 0;
        int c = getopt_long(argc, argv, "hd", long_options, &option_index);

        if (c == -1) break;  // Detect the end of the options.

        switch (c) {
            case 0:
                // for long args without short equivalent that just set a flag
                // nothing left to do so just break.
                if (long_options[option_index].flag != 0) break;
                break;

            case 'd':
                en_debug = true;
                break;

            case 'h':
                _print_usage();
                return true;

            default:
                // Print the usage if there is an incorrect command line option
                _print_usage();
                return true;
        }
    }
    return false;
}


static void _quit(int ret) {
    // Close all the open pipe connections
    pipe_server_close_all();
    pipe_client_close_all();

    // Remove this process ID file from the filesystem so this app can run again latter
    remove_pid_file(PROCESS_NAME);

    if(warp1) mcv_warp_deinit(warp1);
    if(warp2) mcv_warp_deinit(warp2);
    if(warp3) mcv_warp_deinit(warp3);

    // If we are exiting cleanly then say so
    if (ret == 0) {
        printf("Exiting Cleanly\n");
    }

    // Exit with the return code
    exit(ret);
    return;
}


// timing helper
uint64_t rc_nanos_monotonic_time() {
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return ((uint64_t)ts.tv_sec * 1000000000) + ts.tv_nsec;
}


static void _camera_connect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context) {
    printf("Connected to camera server\n");
}


static void _camera_disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context) {
    fprintf(stderr, "Disonnected from camera server\n");
}



static void _tflite_disconnect_cb(__attribute__((unused)) int ch, __attribute__((unused)) void* context) {
    fprintf(stderr, "Disonnected from camera server\n");
    pthread_mutex_lock(&tflite_mutex);
    tflite_detections.clear();
    last_frame_id_processed = -1;
    pthread_mutex_unlock(&tflite_mutex);
}


static int _setup_config(void) {
    printf("loading calibration files\n");

    cv::FileStorage fs(intrinsics_file, cv::FileStorage::READ);
    cv::FileStorage fs_e(extrinsics_file, cv::FileStorage::READ);


    if (!fs.isOpened()) {
        fprintf(stderr, "Failed to load lens cal file %s\n", intrinsics_file);
        return -1;
    }

    if (!fs_e.isOpened()) {
        fprintf(stderr, "Failed to load lens cal file %s\n", intrinsics_file);
        return -1;
    }

    // Read the calib data into the matrices
    cv::FileNode n;

    // pull matrices from file
    n = fs["M1"];
    if (n.type() == cv::FileNode::NONE) {
        fprintf(stderr, "ERROR failed to find M1 in %s\n", intrinsics_file);
    } else
        n >> M1;

    n = fs["D1"];
    if (n.type() == cv::FileNode::NONE) {
        fprintf(stderr, "ERROR failed to find D1 in %s\n", intrinsics_file);
    } else
        n >> D1;

    n = fs["M2"];
    if (n.type() == cv::FileNode::NONE) {
        fprintf(stderr, "ERROR failed to find M2 in %s\n", intrinsics_file);
    } else
        n >> M2;

    n = fs["D2"];
    if (n.type() == cv::FileNode::NONE) {
        fprintf(stderr, "ERROR failed to find D2 in %s\n", intrinsics_file);
    } else
        n >> D2;

    fs.release();

    cv::Mat Rstereo, Tstereo;

    n = fs_e["R"];
    if (n.type() == cv::FileNode::NONE) {
        fprintf(stderr, "ERROR failed to find R in %s\n", extrinsics_file);
    } else
        n >> Rstereo;

    n = fs_e["T"];
    if (n.type() == cv::FileNode::NONE) {
        fprintf(stderr, "ERROR failed to find T in %s\n", extrinsics_file);
    } else
        n >> Tstereo;


    double M0data[9] = {530, 0, 640, 0, 530, 400, 0, 0, 1};  //camera model for output warp
    M0 = cv::Mat(3, 3, CV_64F, M0data);

    double os = 0.39;                  //output scale
    Mzoom = M0.clone();               //camera model for final transform / zoom
    Mzoom.at<double>(0,0) *= os;      //scale focal length
    Mzoom.at<double>(1,1) *= os;

    if (en_debug){
        std::cout << "M0:" << std::endl << M0 << std::endl;
        std::cout << "MZoom:" << std::endl << Mzoom << std::endl;
        std::cout << "M1" << std::endl << M1 << std::endl;
    }

    std::cout << "OG RSTEREO:" << std::endl << Rstereo << std::endl;


    rc_matrix_t R_stereo = RC_MATRIX_INITIALIZER;
    int ret_ = rc_matrix_alloc(&R_stereo, 3, 3);

    for (int i = 0; i < 3; i++){
        for (int j = 0; j < 3; j++){
            R_stereo.d[i][j] = Rstereo.at<double>(j, i);
        }
    }

    rc_vector_t axis;
    double angle;

    int ret = rc_rotation_matrix_to_axis_angle(R_stereo, &axis, &angle);
    angle /= 2;
    
    rc_vector_print(axis);
    fprintf(stderr, "angle: %6.5f\n", angle);

    ret = rc_axis_angle_to_rotation_matrix(axis, angle, &R_stereo);

    cv::Mat R1 = cv::Mat(3, 3, CV_64F, cv::Scalar(0));

    for (int i = 0; i < 3; i++){
        for (int j = 0; j < 3; j++){
            // HEADS UP
            // TRANSPOSING AT THE SAME TIME TO CORRECT FOR RC_MATH
            R1.at<double>(j, i) = R_stereo.d[j][i];
        }
    }

    std::cout << "NEW RSTEREO:" << std::endl << R1 << std::endl;


    cv::Mat R2 = R1.inv();

    // normal scale
    // M1_warp = M1 * R1 * M1.inv();
    // M2_warp = M1 * R2 * M2.inv();

    // zoooomed scale
    M1_warp = Mzoom * R1 * M1.inv();
    M2_warp = Mzoom * R2 * M2.inv();

    M1_warp.convertTo(M1_warp, CV_32F);
    M2_warp.convertTo(M2_warp, CV_32F);

    if (en_debug){
        std::cout << "M1_warp:" << std::endl << M1_warp << std::endl;
        std::cout << "M2_warp:" << std::endl << M2_warp << std::endl;
    }

    // setup for cvp
    if(mcv_pull_stereo_intrinsics(intrinsics_file, lens1, lens2)) return -1;

    printf("\n");
    printf("------ Lens 1 ------\n");
    printf("w:   %d\n", lens1.w);
    printf("h:   %d\n", lens1.h);
    printf("fx: %7.2f\n", lens1.fx);
    printf("fy: %7.2f\n", lens1.fy);
    printf("cx: %7.2f\n", lens1.cx);
    printf("cy: %7.2f\n", lens1.cy);
    for(int i = 0; i < lens1.n_coeffs; i++){
    printf("D%d: %7.4f\n", i, lens1.D[i]);
    }
    printf("\n");

    printf("------ Lens 2 ------\n");
    printf("w:   %d\n", lens2.w);
    printf("h:   %d\n", lens2.h);
    printf("fx: %7.2f\n", lens2.fx);
    printf("fy: %7.2f\n", lens2.fy);
    printf("cx: %7.2f\n", lens2.cx);
    printf("cy: %7.2f\n", lens2.cy);
    for(int i = 0; i < lens2.n_coeffs; i++){
    printf("D%d: %7.4f\n", i, lens2.D[i]);
    }
    printf("\n");

    return 0;
}

cv::Mat img_un1, img_un2, img_warp1, img_warp2;

static void* warp_thread_func(void* context)
{
	uint8_t* ctx = (uint8_t*)context;

    if (*ctx == 0)
        cv::warpPerspective(img_un1,img_warp1,M1_warp,img_un1.size(),cv::INTER_LINEAR, cv::BORDER_TRANSPARENT);
    else if (*ctx == 1)
        cv::warpPerspective(img_un2,img_warp2,M2_warp,img_un1.size(),cv::INTER_LINEAR, cv::BORDER_TRANSPARENT);
    return NULL;
}

static int warp_perspective_async(uint8_t *id, pthread_t* thread){
    return pthread_create(thread, NULL, warp_thread_func, (void*)id);
}

static void _flir_helper_cb(__attribute__((unused)) int ch, camera_image_metadata_t meta, char* frame, void* context) {
    if (pipe_client_bytes_in_pipe(ch)>0){
        fprintf(stderr, "WARNING, skipping flir frame on channel %d due to frame backup\n", ch);
        return;
    }

    if (!en_flir_overlay) return;
    cv::Mat flir_original = cv::Mat(meta.height, meta.width, CV_8UC3, frame);

    // upscale!
    pthread_mutex_lock(&flir_mutex);
    cv::resize(flir_original, flir_mat, flir_mat.size(), cv::INTER_LINEAR);
    pthread_mutex_unlock(&flir_mutex);

    return;
}

static void _tflite_helper_cb(__attribute__((unused))int ch, char* data, int bytes, void* context){
    if (pipe_client_bytes_in_pipe(ch)>0){
        fprintf(stderr, "WARNING, skipping tflite frame on channel %d due to frame backup\n", ch);
        return;
    }

    if (!en_tflite_overlay) return;

    ai_detection_t detection;
    memcpy ( &detection, data, sizeof(ai_detection_t) );

    if (detection.magic_number != AI_DETECTION_MAGIC_NUMBER) return;

    // clear detections vector as we progress, or if we get dcd
    if (last_frame_id_processed < 0 || detection.frame_id > last_frame_id_processed){
        last_frame_id_processed = detection.frame_id;
        pthread_mutex_lock(&tflite_mutex);
        tflite_detections.clear();
        pthread_mutex_unlock(&tflite_mutex);
    }

    // undistort our bbox points since we overlay after the undistort
    std::vector<cv::Point2f> inputDistortedPoints = { cv::Point2f(detection.x_min, detection.y_min), cv::Point2f(detection.x_max, detection.y_max) };
    std::vector<cv::Point2f> outputUndistortedPoints;

    cv::undistortPoints(inputDistortedPoints, outputUndistortedPoints, M2, D2, cv::noArray(), M2);

    pthread_mutex_lock(&tflite_mutex);
    cv::Rect curr_detection = cv::Rect(outputUndistortedPoints[0].x, outputUndistortedPoints[0].y, (outputUndistortedPoints[1].x - outputUndistortedPoints[0].x), (outputUndistortedPoints[1].y - outputUndistortedPoints[0].y));
    tflite_detections.push_back(curr_detection);
    pthread_mutex_unlock(&tflite_mutex);

    return;
}


static void _mavlink_helper_cb(__attribute__((unused))int ch, char* data, int bytes, void* context){
    static uint16_t last_rc_ch = 0;

    if (pipe_client_bytes_in_pipe(ch)>0){
        // printf("WARNING, skipping mavlink packet on channel %d due to frame backup\n", ch);
        return;
    }

    if (bytes != sizeof(mavlink_message_t)) return;
    mavlink_message_t* msg = (mavlink_message_t*)data;

    if (msg->msgid != MAVLINK_MSG_ID_RC_CHANNELS ) return;

    uint16_t raw_ch = rc_parser(msg);

    if (last_rc_ch != 0 && last_rc_ch == raw_ch){
        return;
    }

    if (raw_ch <= CHANNEL_STITCHED) curr_mode = STITCHED;
    else if (raw_ch <= CHANNEL_NO_FLIR) curr_mode = NO_FLIR;
    else if (raw_ch >= CHANNEL_FLIR) curr_mode = FLIR;

    last_rc_ch = raw_ch;
}


static void _stereo_camera_helper_cb(__attribute__((unused)) int ch, camera_image_metadata_t meta, char* frame, void* context) {
    uint8_t *outbuffer1, *outbuffer2, *outbuffer3;
    cv::Mat output_img;

    if (pipe_client_bytes_in_pipe(ch)>0){
        // fprintf(stderr, "WARNING, skipping frame on channel %d due to frame backup\n", ch);
        return;
    }

    if (meta.format != IMAGE_FORMAT_STEREO_NV12){
        fprintf(stderr, "ERROR: only support stereo nv images\n");
        return;
    }

    switch (curr_mode)
    {
    case STITCHED:{

        // cvp undistort
        if (int code = mcv_warp_process(warp1, (uint8_t*)frame, &outbuffer1)) {
            fprintf(stderr, "ERROR: mcv_warp_process(1) returned error: ");
            mcv_print_error(code);
            return;
        }

        if (int code = mcv_warp_process(warp2, (uint8_t*)&frame[meta.size_bytes/2], &outbuffer2)) {
            fprintf(stderr, "ERROR: mcv_warp_process(2) returned error: ");
            mcv_print_error(code);
            return;
        }

        // create cv mat objects, color convert
        cv::Mat yuv1 = cv::Mat(height + height / 2, width, CV_8UC1, (uchar*)outbuffer1);
        cv::cvtColor(yuv1, img_un1, CV_YUV2RGB_NV12);

        cv::Mat yuv2 = cv::Mat(height + height / 2, width, CV_8UC1, (uchar*)outbuffer2);
        cv::cvtColor(yuv2, img_un2, CV_YUV2RGB_NV12);

        img_warp1 = img_un1.clone();
        img_warp1.setTo(cv::Vec3b(0,0,0));
        img_warp2 = img_un2.clone();
        img_warp2.setTo(cv::Vec3b(0,0,0));

        pthread_t thread1, thread2;

        static uint8_t id = 0;
        static uint8_t id2 = 1;

        // BORDER_TRANSPARENT gets rid of black edge artifacts at the overlap
        warp_perspective_async(&id, &thread1);
        warp_perspective_async(&id2, &thread2);

        pthread_join(thread1, NULL);
        pthread_join(thread2, NULL);

        // blending should be re-written to just merge the two images instead of doing 0.5 alpha blend and then
        // multiplying pixels that came from single image by 2
        double alpha = 0.5; double beta = ( 1.0 - alpha );
        cv::addWeighted( img_warp1, alpha, img_warp2, beta, 0.0, output_img);

        for (int ii=0; ii<output_img.size[0]; ii++){
            for (int jj=0; jj<output_img.size[1]; jj++){
                if (img_warp1.at<cv::Vec3b>(ii,jj) != cv::Vec3b(0,0,0)) output_img.at<cv::Vec3b>(ii,jj) = img_warp1.at<cv::Vec3b>(ii,jj);
                else if (img_warp2.at<cv::Vec3b>(ii,jj) != cv::Vec3b(0,0,0)) output_img.at<cv::Vec3b>(ii,jj) = img_warp2.at<cv::Vec3b>(ii,jj);
            }
        }
    }
        break;

    case NO_FLIR:{
        // cvp undistort
        if (int code = mcv_warp_process(warp1, (uint8_t*)frame, &outbuffer1)) {
            fprintf(stderr, "ERROR: mcv_warp_process(1) returned error: ");
            mcv_print_error(code);
            return;
        }

        cv::Mat yuv1 = cv::Mat(height + height / 2, width, CV_8UC1, (uchar*)outbuffer1);
        cv::cvtColor(yuv1, output_img, CV_YUV2RGB_NV12);

        pthread_mutex_lock(&tflite_mutex);
        for (int i = 0; i < tflite_detections.size(); i++){
            cv::rectangle(output_img, tflite_detections[i], cv::Scalar(255, 0, 0), 2);
        }
        pthread_mutex_unlock(&tflite_mutex);    
    }
        break;

    case FLIR:{
        // cvp undistort
        if (int code = mcv_warp_process(warp1, (uint8_t*)frame, &outbuffer1)) {
            fprintf(stderr, "ERROR: mcv_warp_process(1) returned error: ");
            mcv_print_error(code);
            return;
        }

        cv::Mat yuv1 = cv::Mat(height + height / 2, width, CV_8UC1, (uchar*)outbuffer1);
        cv::cvtColor(yuv1, output_img, CV_YUV2RGB_NV12);

        cv::Mat destRoi;
        pthread_mutex_lock(&flir_mutex);
        destRoi = output_img(cv::Rect(width/2-flir_mat.cols/2, height/2-flir_mat.rows/2, flir_mat.cols, flir_mat.rows));
        flir_mat.copyTo(destRoi);
        pthread_mutex_unlock(&flir_mutex);
    }
        break;
    
    default:
        fprintf(stderr, "ERROR: unsupported mode provided!\n");
        return;
    }

    meta.format = IMAGE_FORMAT_RGB;
    meta.height = output_img.rows;
    meta.width = output_img.cols;
    meta.size_bytes = meta.width * meta.height * 3;

    pipe_server_write_camera_frame(IMAGE_CH, meta, output_img.data);

    return;
}

static int _setup_client_pipes(void) {
    int flags = CLIENT_FLAG_EN_CAMERA_HELPER;

    int ch = pipe_client_get_next_available_channel();

    pipe_client_set_camera_helper_cb(ch, _stereo_camera_helper_cb, NULL);
    pipe_client_set_connect_cb(ch, _camera_connect_cb, NULL);
    pipe_client_set_disconnect_cb(ch, _camera_disconnect_cb, NULL);

    int ret = pipe_client_open(ch, input_pipe, PROCESS_NAME, flags, 0);
    if (ret < 0) {
        fprintf(stderr, "ERROR: critical failure subscribing to %s pipe\n", input_pipe);
        pipe_print_error(ret);
        return -1;
    }

    ch = pipe_client_get_next_available_channel();

    pipe_client_set_camera_helper_cb(ch, _flir_helper_cb, NULL);
    pipe_client_set_connect_cb(ch, _camera_connect_cb, NULL);
    pipe_client_set_disconnect_cb(ch, _camera_disconnect_cb, NULL);

    ret = pipe_client_open(ch, FLIR_PIPE, PROCESS_NAME, flags, 0);
    if (ret < 0) {
        fprintf(stderr, "ERROR: critical failure subscribing to flir pipe\n");
        pipe_print_error(ret);
        return -1;
    }


    ch = pipe_client_get_next_available_channel();

    pipe_client_set_simple_helper_cb(ch, _tflite_helper_cb, NULL);
    pipe_client_set_connect_cb(ch, _camera_connect_cb, NULL);
    pipe_client_set_disconnect_cb(ch, _tflite_disconnect_cb, NULL);

    ret = pipe_client_open(ch, DET_PIPE, PROCESS_NAME, EN_PIPE_CLIENT_SIMPLE_HELPER, sizeof(ai_detection_t) * 64);
    if (ret < 0) {
        fprintf(stderr, "ERROR: critical failure subscribing to tflite pipe\n");
        pipe_print_error(ret);
        return -1;
    }

    if (rc_parser != nullptr){
        ch = pipe_client_get_next_available_channel();

        pipe_client_set_simple_helper_cb(ch, _mavlink_helper_cb, NULL);

        ret = pipe_client_open(ch, MAVLINK_IO_PATH, PROCESS_NAME, EN_PIPE_CLIENT_SIMPLE_HELPER, sizeof(mavlink_message_t) * 5);
        if (ret < 0) {
            fprintf(stderr, "ERROR: critical failure subscribing to mavlink pipe\n");
            pipe_print_error(ret);
            return -1;
        }
    }

    return 0;
}

static int _setup_server_pipes(void) {
    int flags = 0;

    // image pipe
    pipe_info_t info1 = {
        {"stereo_stitched"},           // name
        {"/run/mpa/stereo_stitched"},  // location
        "camera_image_metadata_t",     // type
        PROCESS_NAME,                  // server_name
        (8 * 1024 * 1024),             // size_bytes
        0                              // server_pid
    };

    if (pipe_server_create(IMAGE_CH, info1, flags)) return -1;

    return 0;
}

int main(int argc, char* argv[]) {
    // Parse the command line options and terminate if the parser says we should terminate
    if (_parse_opts(argc, argv)) {
        return -1;
    }

    // load and print config file
    if (config_file_read()) {
        return -1;
    }
    config_file_print();

    /* make sure another instance isn't running
     * if return value is -3 then a background process is running with
     * higher privileges and we couldn't kill it, in which case we should
     * not continue or there may be hardware conflicts. If it returned -4
     * then there was an invalid argument that needs to be fixed.
     */
    if (kill_existing_process(PROCESS_NAME, 2.0) < -2) {
        // Exit the app with an error code
        exit(-1);
    }

    // start signal handler so we can exit cleanly
    if (enable_signal_handler() == -1) {
        fprintf(stderr, "ERROR: failed to start signal handler\n");
        exit(-1);
    }

    /* make PID file to indicate your project is running
     * due to the check made on the call to rc_kill_existing_process() above
     * we can be fairly confident there is no PID file already and we can
     * make our own safely.
     */
    make_pid_file(PROCESS_NAME);

    cpu_set_t cpuset;

    /* Set affinity mask to include CPUs 7 only */
    CPU_ZERO(&cpuset);
    CPU_SET(6, &cpuset);
    CPU_SET(5, &cpuset);
    CPU_SET(4, &cpuset);

    if(sched_setaffinity(0, sizeof(cpu_set_t), &cpuset)){
        perror("sched_setaffinity");
    }

    /* Check the actual affinity mask assigned to the thread */
    if(sched_getaffinity(0, sizeof(cpu_set_t), &cpuset)){
        perror("sched_getaffinity_np");
    }
    printf("voxl-stitcher is now locked to the following cores:");
    for (int j = 0; j < CPU_SETSIZE; j++){
        if(CPU_ISSET(j, &cpuset)) printf(" %d", j);
    }
    printf("\n");

    // disable garbage multithreading
    // cv::setNumThreads(1);

   // load in calibration files
	if(_setup_config()){
        printf("Error: failed to parse calibration files\n");
        _quit(-1);
    }

    mcv_warp_config_t conf1 = {
        lens1.w,
        lens1.h,
        1,
        WARP_GRID,
        lens1
    };
    mcv_warp_config_t conf2 = {
        lens2.w,
        lens2.h,
        1,
        WARP_GRID,
        lens2
    };

    // only necessary for perspective transform
    // memcpy(conf1.transform, M1_warp.data, 9* sizeof(float));
    // memcpy(conf2.transform, M2_warp.data, 9* sizeof(float));

    if(!(warp1 = mcv_warp_init(conf1))){
        printf("Error: failed to initialize warp 1\n");
        _quit(-1);
    }
    if(!(warp2 = mcv_warp_init(conf2))){
        printf("Error: failed to initialize warp 2\n");
        _quit(-1);
    }

	// set up pipes, client-interface starts paused until we get our own subscriber
	if(_setup_client_pipes()){
        printf("Error: failed to setup client pipes\n");
        _quit(-1);
    }
	if(_setup_server_pipes()){
        printf("Error: failed to setup server pipes\n");
        _quit(-1);
    }

	// wait for signal to close
	main_running = 1;
	while(main_running){
		usleep(5000000);
	}

	// exit cleanly
	_quit(0);
	return 0;
}
